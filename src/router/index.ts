import { createRouter, createWebHistory } from 'vue-router';
import ProductDetail from '../components/ProductDetail.vue';

const routes = [
  {
    path: '/products/:productName',
    name: 'ProductDetail',
    component: ProductDetail,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
