import { reactive } from 'vue';

interface Product {
  name: string;
  slug: string;
  image: string;
}

interface Store {
  searchQuery: string;
  products: Product[];
  filteredProducts: Product[];
}

export const store = reactive<Store>({
  searchQuery: '',
  products: [
    { name: 'Posters', slug: 'posters', image: '/img/posters.png' },
    { name: 'Flyers', slug: 'flyers', image: '/img/flyers.png' },
    { name: 'Business Cards', slug: 'business-cards', image: '/img/business-cards.png' },
  ],
  get filteredProducts() {
    return this.products.filter(product =>
      product.name.toLowerCase().includes(this.searchQuery.toLowerCase())
    );
  }
});
